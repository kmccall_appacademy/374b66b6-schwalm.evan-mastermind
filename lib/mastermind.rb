class Code
  PEGS = {
    "R" => :red,
    "G" => :green,
    "B" => :blue,
    "Y" => :yellow,
    "O" => :orange,
    "P" => :purple
  }

  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(str)
    pegs = str.upcase.split("").map do |color|
      raise "parse error" unless PEGS.key?(color)
      PEGS[color]
    end

    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample }

    Code.new(pegs)
  end

  def [](idx)
    pegs[idx]
  end

  def exact_matches(answer)
    count = 0
    pegs.each_index do |i|
      count += 1 if self[i] == answer[i]
    end

    count
  end

  def near_matches(answer)
    answer_count = answer.color_count

    near_matches = 0
    self.color_count.each do |color, count|
      next unless answer_count.key?(color)

      near_matches += [count, answer_count[color]].min
    end

    near_matches - self.exact_matches(answer)
  end

  def color_count
    counts = Hash.new(0)

    @pegs.each do |color|
      counts[color] += 1
    end

    counts
  end

  def ==(answer)
    return false unless answer.is_a?(Code)

    self.pegs == answer.pegs
  end
end

class Game
  MAX_TURNS = 10

  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def play
    MAX_TURNS.times do

      guess = get_guess

      if guess == @secret_code
        puts "You Win!"
        return
      end

      display_matches(guess)
    end

    puts "You lose :("
  end

  def get_guess
    puts "Guess the secret code:"

    begin
      Code.parse(gets.chomp)
    rescue
      "Parse error!"
      retry
    end
  end

  def display_matches(guess)
    exact = @secret_code.exact_matches(guess)
    near = @secret_code.near_matches(guess)

    puts "You got #{exact} exact matches!"
    puts "You got #{near} near matches!"
  end

end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
